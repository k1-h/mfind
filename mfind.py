#!/usr/bin/python
'''
Author: Keyvan Hedayati <k1.hedayati93@gmail.com>
License: GNU General Public License, version 2
TODO: Add fuzzy matching
'''
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from argparse import RawDescriptionHelpFormatter, REMAINDER
from logging import basicConfig, debug, error, info, critical
from os import linesep, remove, listdir
from os.path import abspath, dirname, join, isfile, splitext
from random import randrange
from re import split
from subprocess import check_output, CalledProcessError, call
from sys import stdin

from magic import from_file


def main():
    '''
    Parses arguments.
    Gets list of files bases on choosed option.
    Makes a playlist from returned list.
    Finally plays the playlist.
    '''
    args = parse_args()
    log_format = '%(levelname)s - %(funcName)s: %(message)s'
    basicConfig(level=args.verbose.upper(), format=log_format)
    debug(args)
    file_list = get_file_list(args)
    play_list = make_playlist(file_list, args.only, args.sort, list(args.type))
    play_files(play_list, args.margs)


def parse_args():
    ''' Parses command-line arguments. '''
    description = 'mplayer easy playlist maker'
    formatter_class = (ArgumentDefaultsHelpFormatter and
                       RawDescriptionHelpFormatter)
    examples = '''
Examples:
    mfind.py -s the big bang theory s06 e13
        First search for "the big bang theory s06 e13" with "locate"
        then if "locate" found something play it with all other files
        in that directory
    mfind.py -m'-vo null' -f /path/to/video/file
        Play a video file without video, just sound
    find . -name '*.mp4' | mfind.py -p
        Play all mp4 file in this directory
    mfind.py -of /just/play/this/file
        Just play specified and not other files in "/just/play/this/"
    mfind.py -f /path/to/media/file
        Play all media files in "/path/to/media" starts from "file"
    mfind.py -f /path/to/media/directory/
        Play all media files in "/path/to/media/directory/"
    mfind.py -f /list/of/files/file1 /list/of/files2/file2
        Play all specified files
    mfind.py -t v music
        Play just video files that have 'music' in their name, not audio files
    mfind.py -f .
        Play all files in current directory
    mfind.py -f *
        Plays all files in current directory
'''
    parser = ArgumentParser(description=description,
                            formatter_class=formatter_class,
                            epilog=examples)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-s',
                       '--search',
                       help='Search file with locate',
                       nargs=REMAINDER)
    group.add_argument('-f',
                       '--file',
                       help='Media file path',
                       nargs=REMAINDER)
    group.add_argument('-p',
                       '--pipe',
                       action='store_true',
                       help='Input list directly from pipe, usefull for\
                             piping "find" or "locate" output.')
    parser.add_argument('-m',
                        '--margs',
                        default='--loop=inf',
                        help='Args to pass mplayer',
                        metavar='mplayer args',
                        type=str)
    parser.add_argument('-l',
                        '--largs',
                        default='-eiA',
                        help='Args to pass locate',
                        metavar='locate args',
                        type=str,)
    parser.add_argument('-o',
                        '--only',
                        action='store_true',
                        help='Only play specified file')
    parser.add_argument('-S',
                        '--sort',
                        action='store_true',
                        help='Sort final play list')
    parser.add_argument('-t',
                        '--type',
                        action='store',
                        choices=['a', 'v'],
                        default=['a', 'v'],
                        help='Media file type to play, audio or video')
    #parser.add_argument('-i',
    #                    '--include',
    #                    help='Include directories from list',
    #                    action='store_true')
    parser.add_argument('-v',
                        '--verbose',
                        choices=['debug', 'info', 'warning', 'error',
                                 'critical'],
                        default='error',
                        help='Log level')

    return parser.parse_args()


def get_file_list(args):
    '''
    Gets list of files, if search selected uses locate command to find files
    otherwise just puts input argument in file_list.
    '''
    if args.search:
        command = ['/usr/bin/locate', args.largs] + args.search
        info('Excuting ' + ' '.join(command))
        try:
            file_list = (check_output(command, universal_newlines=True)
                         .split(linesep)[:-1])
        except CalledProcessError:
            error('File not found')
            exit(1)
    elif args.pipe:
        file_list = stdin.read().split(linesep)[:-1]
    elif args.file:
        file_list = args.file
    debug('Input list: ' + linesep + linesep.join(file_list))

    return file_list


def make_playlist(file_list, only, sort, type):
    '''
    Main function, this is not best way but it works. Any suggestion?
    If file_list points to only one file or directory then adds all files in
    that directory to play_list then finds first file that should play, if
    file_list points to multiple files then just play them all. After created
    play_list if it was empty show error and exits script else returns list.
    '''
    play_list = []

    if only or len(file_list) == 1 or isinstance(file_list, str):
        for file_par in file_list:
            dir_name = abspath(dirname(file_par))
            for file in sort_nicely(listdir(dir_name)):
                file_abs = join(dir_name, file)
                if isfile(file_abs) and is_media(file_abs, type):
                    play_list.append(file_abs)
        first_file = abspath(file_list[0])
        if first_file in play_list:
            start = play_list.index(first_file)
            play_list += play_list[:start]
            del play_list[:start]
    else:
        for file in file_list:
            file_abs = abspath(file)
            if (isfile(file_abs) and is_media(file_abs, type)):
                play_list.append(file_abs)

    if len(play_list) == 0:
        error('File not found')
        exit(1)

    if sort:
        play_list.sort(key=str.lower)
    info('Play list: ' + linesep + linesep.join(play_list))
    return play_list


def sort_nicely(list):
    ''' Human sort '''
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in split('([0-9]+)', key)]
    list.sort(key=alphanum_key)
    return list


def is_media(file_path, type):
    '''
    Checks if filepath is media file with magic module and extention
    check for most accuracy. For performance issues first checks file
    extention if check failed then uses magic module.
    '''
    media_exts = dict.fromkeys([
        '.acc', '.amr', '.asf', '.fla', '.flac', '.m4a', '.m4v', '.mid',
        '.mp2', '.mp3', '.ogg', '.ra', '.wav', '.wma'
    ], 'a')
    media_exts.update(dict.fromkeys([
        '.3gp', '.aaf', '.avi', '.cam', '.dat', '.f4v', '.flv', '.ogv',
        '.m4v', '.mkv', '.mov', '.mp4', '.mpeg', '.mpg', '.rm', '.ts',
        '.webm', '.wmv'
    ], 'v'))
    file_ext = splitext(file_path)[1].lower()
    file_type = None

    if file_ext in media_exts:
        file_type = media_exts[file_ext]
    else:
        mime_type = from_file(file_path, mime=True).decode('utf-8')
        debug(mime_type + '   ' + file_path)
        if mime_type.startswith('audio'):
            file_type = 'a'
        elif mime_type.startswith('video'):
            file_type = 'v'

    return file_type in type


def play_files(list, margs):
    '''
    Wirtes list to a file then plays the wrritten playlist with mplayer when
    finished removes playlist_file.
    '''
    playlist_file = write_list_to_file(list)
    command = [
        '/usr/bin/mpv',
        '-playlist',
        playlist_file
    ] + margs.split(' ')
    info('Excuting ' + ' '.join(command))
    try:
        call(command, restore_signals=False)
    except KeyboardInterrupt:
        info('Keyboard Interrput, Bye.')
    remove(playlist_file)


def write_list_to_file(input_list):
    '''
    Writes input list to a temporary file and returns file name.
    tempfile module does not have errors='surrogateescape' arg.
    '''
    temp_file = open('.' + str(randrange(999999)), mode='w',
                     errors='surrogateescape')
    temp_file.write(linesep.join(input_list))
    temp_file.close()
    return temp_file.name


if __name__ == '__main__':
    main()
