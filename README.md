###Description
Play all files from "locate" or "find" output and or all media files in a folder with mplayer.
Inspired form "Play all files in folder" in "The KMPlayer" and "Everything" fast search and merge this feature.

###Requirements
python-magic module, install it with:

    pip install python-magic
or download from:
https://github.com/ahupp/python-magic

###Specifications
+ Uses subprocess, logging, re, tempfile, magic and argparse modules

###Examples
mfind.py -s the big bang theory s06 e13
    First search for "the big bang theory s06 e13" with "locate" then if "locate" found something play it with all other files in that directory
mfind.py -m'-vo null' -f /path/to/video/file
    Play a video file without video, just sound
find . -name '*.mp4' | mfind.py -p
    Play all mp4 file in this directory
mfind.py -of /just/play/this/file
    Just play specified and not other files in "/just/play/this/"
mfind.py -f /path/to/media/file
    Play all media files in "/path/to/media" starts from "file"
mfind.py -f /path/to/media/directory/
    Play all media files in "/path/to/media/directory/"
mfind.py -f /list/of/files/file1 /list/of/files2/file2
    Play all specified files
mfind.py -t v music
    Play just video files that have 'music' in their name, not audio files
mfind.py -f .
    Play all files in current directory
mfind.py -f *
    Plays all files in current directory

###How to use
    usage: mfind [-h] (-s ... | -f ... | -p) [-m mplayer args] [-l locate args]
                 [-o] [-S] [-t {a,v}] [-v {debug,info,warning,error,critical}]

    mplayer easy playlist maker

    optional arguments:
      -h, --help            show this help message and exit
      -s ..., --search ...  Search file with locate
      -f ..., --file ...    Media file path
      -p, --pipe            Input list directly from pipe, usefull for piping
                            "find" or "locate" output.
      -m mplayer args, --margs mplayer args
                            Args to pass mplayer
      -l locate args, --largs locate args
                            Args to pass locate
      -o, --only            Only play specified file
      -S, --sort            Sort final play list
      -t {a,v}, --type {a,v}
                            Media file type to play, audio or video
      -v {debug,info,warning,error,critical}, --verbose {debug,info,warning,error,critical}
                            Log level

###Tips
* See my mplayer config and input file too:
        https://github.com/k1-hedayati/linux-config-files/tree/master/mplayer
* Copy mfind to /usr/bin/ for convinience:

        sudo cp mfind.py /usr/bin/

* Edit mplayer.desktop to use mfind insted of mplayer:

        nano ~/.local/share/applications/mplayer.desktop

    and find this line:

        Exec=mplayer %F

    change it with:

        Exec=mfind.py -f %F

    save & exit

* Use alias

        alias m='mfind.py'
